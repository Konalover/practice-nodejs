const jwt = require("jsonwebtoken");
const express = require('express');
const router = express.Router();
const models = require('../models');
const app = require('../app');

router.post('/', (req, res) => {
    const {name, password} = req.body;

    models.User
        .findOne({name})
        .then(user => {
            if (user) {
              return res.status(400).send('User already exist');
            }
            models.User
              .create({name, password})
              .then(user => {
                  const name = user.name;
                  res.json({ token:   jwt.sign({name}, 'superSecret', { // create a token
                        expiresIn: "24h" // expires in 24 hours
                    })
                  });
              })
        })
        .catch(err => console.log(err));
});

router.post('/singedIn', (req, res) => {
    const {name, password} = req.body;

    models.User
        .findOne({name, password})
        .then(user => {
            const name = user.name;
            res.json({ token:   jwt.sign({name}, 'superSecret', { // create a token
                  expiresIn: "24h" // expires in 24 hours
              }) });
        })
        .catch(err => console.log(err));
});

module.exports = router;
